const styles = function (settings) {
    return {
        chat: [
            `transition: 0.30s`,
            `height: 0`,
            `background-size: cover`,
            `z-index: 999999`,
            `overflow: hidden`,
            `display: none`,
            `border: ${settings.isMobile ? '1' : '1'}px solid lightgrey`,
            `background-image: ${settings.button.imageURL}`,
            `width: ${settings.isMobile ? '100%' : settings.chat.width}`,
            `margin: ${settings.isMobile ? '0' : `${settings.button.padding}px`}`,
            `top: ${settings.isMobile ? '0' : `auto`}`,
            `transition-timing-function: ease-in`,
            `transition-timing-function: cubic-bezier(0.42, 0, 1, 1)`,
            `background-color: white`
        ].join('; '),
        button: [
            `position: absolute`,
            `bottom: 0`,
            `${settings.position.horizontalAlignment || 'right'}: 0`,
            `background-size: cover`,
            //`border-radius: 25px`,
            `outline: none`,
            //`border: 1px solid #c3c3c3`,
            `border: none`,
            `cursor: pointer`,
            `background-image: ${settings.button.image}`,
            `height: ${settings.button.height ? settings.button.height + 'px' : '50px'}`,
            `margin: ${settings.button.padding}px`,
            `width: ${settings.button.width ? settings.button.width + 'px' : '50px'}`,
            `transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1)`,
            `transition: 0.25s`,
            `background-color: transparent`
        ].join('; '),
        wrapper: [
            `position: fixed`,
            `flex-direction: row`,
            `text-align: ${settings.position.horizontalAlignment}`,
            `${settings.position.horizontalAlignment}: 0`,
            `${settings.position.verticalAlignment}: 0`,
            `width: ${settings.isMobile ? '100%' : 'auto'}`,
            `box-sizing: border-box`,
            `height: auto`,
            `z-index: 999999`,

        ].join('; '),
        closeButton: [
            `position: ${settings.isMobile ? 'absolute' : 'absolute'}`,
            `${settings.closeButtonPosition}: ${settings.isMobile ? '15px' : '35px'} `,
            `top: ${settings.isMobile ? 'calc(33px - 12.5px)' : '36px'}`,
            `width: 25px`,
            `height: 25px`,
            `border-radius: 20px`,
            `color: white`,
            `display: none`,
            `font-size: 22px`,
            `cursor: pointer`,
            `outline: none`
        ].join('; ')
    };
}

class LeoChat {

    constructor(config) {
        this._config = config;
        this.settings = this.loadConfig(config);

        this.wrapper = document.createElement('div');
        this.wrapper.setAttribute('id', 'chat-wrapper');

        let wrapperStyles = styles(this.settings).wrapper;
        this.wrapper.setAttribute('style', wrapperStyles)


        this.closeButton = this.createCloseButton();
        this.wrapper.appendChild(this.closeButton);

        let closeButtonFunction = function () {
            this.buttonClick();
        };

        this.closeButton.onclick = closeButtonFunction.bind(this);

        window.onresize = function(event) {
            this.chat.style.height = `${this.resizeChatHeight()}px`;
        }.bind(this);
    }

    createCloseButton() {
        let closeButton = document.createElement('div');
        closeButton.appendChild(document.createTextNode('X'));
        closeButton.setAttribute('style', styles(this.settings).closeButton);
        return closeButton;
    }

    loadConfig(config) {
        return {
            autoOpen: config.autoOpen || false,
            openDelay: config.openDelay || 5,
            chatURL: config.chatURL || '',
            position: {
                horizontalAlignment: config.position.horizontalAlignment || 'right',
                verticalAlignment: config.position.verticalAlignment || 'bottom'
            },
            button: {
                padding: config.button.padding || 0,
                elementId: config.button.elementId || 'leochat',
                label: config.button.label || 'Chat',
                imageURL: config.button.imageURL,
                width: config.button.width,
                height: config.button.height,
                image: `url(${config.button.image})`,
            },
            closeButtonPosition: config.closeButtonPosition || 'left',
            chat: {
                height: this.isMobile() ? `${this.screenSize().height}px` : `${Math.min(config.chat.height || 500, this.screenSize().height - 30)}px`,
                width: `${config.chat.width}px` || '300px'
            },
            isMobile: this.isMobile(),
            styles: styles,
            screen: this.screenSize()
        }
    }

    screenSize () {
        return {
            width: Math.max(document.documentElement.clientWidth, window.innerWidth || 0),
            height: Math.max(document.documentElement.clientHeight, window.innerHeight || 0)
        }
    }

    resizeChatHeight () {
        return  this.isMobile() ? this.screenSize().height : Math.min(this._config.chat.height || 500, this.screenSize().height - 30)
    }

    createButton() {
        let button = document.createElement('button');

        let buttonStyles = this.settings.styles(this.settings).button;

        button.setAttribute('style', buttonStyles);

        this.wrapper.appendChild(button);

        button.setAttribute('id', this.settings.button.elementId);

        // button.appendChild(document.createTextNode(this.settings.button.label));

        let buttonClicked = (forceOpen) => {

            if (!this.isChatCreated()) {
                this.createChat(true);
                setTimeout(() => {
                    this.sendMessageToIFrame(isChatClosed);
                }, 1000)
            }

            let isChatClosed = this.chat.style.display === 'none';

            if (typeof forceOpen === 'boolean' && forceOpen && !isChatClosed) return;

            if (isChatClosed) {
                this.chat.style.display = 'flex';
                this.closeButton.style.display = 'flex';
                this.button.style.opacity = '0';
                this.button.style.transform = `translateY(80%)`;

                setTimeout(() => {
                    this.chat.style.height = this.settings.chat.height;
                });
            } else {
                this.chat.style.height = '0';
                this.closeButton.style.display = 'flex';
                setTimeout(() => {
                    this.button.style.opacity = '1';
                    this.button.style.transform = `translateY(0)`;
                    this.chat.style.display = 'none';
                }, 300);
            }

            this.sendMessageToIFrame(isChatClosed);
        }

        button.onclick = buttonClicked.bind(this);
        this.buttonClick = buttonClicked.bind(this);

        return button;
    }

    sendMessageToIFrame(isChatClosed) {
        this.chat.contentWindow.postMessage(`chat open status: ${isChatClosed}`, '*')
    }

    isChatCreated() {
        if (this.wrapper && this.wrapper.children) {

            let childrenArray = Array.prototype.slice.call(this.wrapper.children);

            return !!childrenArray.find(el => el.nodeName === `IFRAME`);
        }
        return false;
    }

    isMobile() {
        return Math.max(document.documentElement.clientWidth, window.innerWidth || 0) < 769;
    }

    chatLoadingState = () => (state) => {
    }

    createChat(addToWrapper) {
        let iframe = document.createElement('iframe');

        let styles = this.settings.styles(this.settings).chat;

        iframe.setAttribute('style', styles);

        iframe.src = this.settings.chatURL;
        iframe.name = "chat";

        iframe.onload = this.chatLoadingState().bind(this);


        if (this.settings.autoOpen) {
            setTimeout(() => this.buttonClick(true), this.settings.openDelay * 1000);
        }

        if (addToWrapper) {
            this.appendChatToWrapper()
        }

        return iframe;
    }

    appendChatToWrapper() {
        if (this.settings.position.verticalAlignment === 'top') {
            this.wrapper.appendChild(this.chat)
        } else {
            this.wrapper.prepend(this.chat);
        }
    }

    start() {
        let onDocumentLoad = () => {
            this.button = this.createButton();
            this.chat = this.createChat();

            // if (this.isMobile()) {
            //     this.settings.chat.height = `100%`;
            // }

            this.wrapper.appendChild(this.button)

            document.body.appendChild(this.wrapper)
        };

        window.onload = onDocumentLoad.bind(this)
    }
}