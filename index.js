"use strict";

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var styles = function styles(settings) {
    return {
        chat: ["transition: 0.30s", "height: 0", "background-size: cover", "z-index: 999999", "overflow: hidden", "display: none", "border: ".concat(settings.isMobile ? '1' : '1', "px solid lightgrey"), "background-image: ".concat(settings.button.imageURL), "width: ".concat(settings.isMobile ? '100%' : settings.chat.width), "margin: ".concat(settings.isMobile ? '0' : "".concat(settings.button.padding, "px")), "top: ".concat(settings.isMobile ? '0' : "auto"), "transition-timing-function: ease-in", "transition-timing-function: cubic-bezier(0.42, 0, 1, 1)", "background-color: white"].join('; '),
        button: ["position: absolute", "bottom: 0", "".concat(settings.position.horizontalAlignment || 'right', ": 0"), "background-size: cover", //`border-radius: 25px`,
            "outline: none", //`border: 1px solid #c3c3c3`,
            "border: none", "cursor: pointer", "background-image: ".concat(settings.button.image), "height: ".concat(settings.button.height ? settings.button.height + 'px' : '50px'), "margin: ".concat(settings.button.padding, "px"), "width: ".concat(settings.button.width ? settings.button.width + 'px' : '50px'), "transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1)", "transition: 0.25s", "background-color: transparent"].join('; '),
        wrapper: ["position: fixed", "flex-direction: row", "text-align: ".concat(settings.position.horizontalAlignment), "".concat(settings.position.horizontalAlignment, ": 0"), "".concat(settings.position.verticalAlignment, ": 0"), "width: ".concat(settings.isMobile ? '100%' : 'auto'), "box-sizing: border-box", "height: auto", "z-index: 999999"].join('; '),
        closeButton: ["position: ".concat(settings.isMobile ? 'absolute' : 'absolute'), "".concat(settings.closeButtonPosition, ": ").concat(settings.isMobile ? '15px' : '35px', " "), "top: ".concat(settings.isMobile ? 'calc(33px - 12.5px)' : '36px'), "width: 25px", "height: 25px", "border-radius: 20px", "color: white", "display: none", "font-size: 22px", "cursor: pointer", "outline: none"].join('; ')
    };
};

var LeoChat =
    /*#__PURE__*/
    function () {
        function LeoChat(config) {
            _classCallCheck(this, LeoChat);

            _defineProperty(this, "chatLoadingState", function () {
                return function (state) {};
            });

            this._config = config;
            this.settings = this.loadConfig(config);
            this.wrapper = document.createElement('div');
            this.wrapper.setAttribute('id', 'chat-wrapper');
            var wrapperStyles = styles(this.settings).wrapper;
            this.wrapper.setAttribute('style', wrapperStyles);
            this.closeButton = this.createCloseButton();
            this.wrapper.appendChild(this.closeButton);

            var closeButtonFunction = function closeButtonFunction() {
                this.buttonClick();
            };

            this.closeButton.onclick = closeButtonFunction.bind(this);

            window.onresize = function (event) {
                this.chat.style.height = "".concat(this.resizeChatHeight(), "px");
            }.bind(this);
        }

        _createClass(LeoChat, [{
            key: "createCloseButton",
            value: function createCloseButton() {
                var closeButton = document.createElement('div');
                closeButton.appendChild(document.createTextNode('X'));
                closeButton.setAttribute('style', styles(this.settings).closeButton);
                return closeButton;
            }
        }, {
            key: "loadConfig",
            value: function loadConfig(config) {
                return {
                    autoOpen: config.autoOpen || false,
                    openDelay: config.openDelay || 5,
                    chatURL: config.chatURL || '',
                    position: {
                        horizontalAlignment: config.position.horizontalAlignment || 'right',
                        verticalAlignment: config.position.verticalAlignment || 'bottom'
                    },
                    button: {
                        padding: config.button.padding || 0,
                        elementId: config.button.elementId || 'leochat',
                        label: config.button.label || 'Chat',
                        imageURL: config.button.imageURL,
                        width: config.button.width,
                        height: config.button.height,
                        image: "url(".concat(config.button.image, ")")
                    },
                    closeButtonPosition: config.closeButtonPosition || 'left',
                    chat: {
                        height: this.isMobile() ? "".concat(this.screenSize().height, "px") : "".concat(Math.min(config.chat.height || 500, this.screenSize().height - 30), "px"),
                        width: "".concat(config.chat.width, "px") || '300px'
                    },
                    isMobile: this.isMobile(),
                    styles: styles,
                    screen: this.screenSize()
                };
            }
        }, {
            key: "screenSize",
            value: function screenSize() {
                return {
                    width: Math.max(document.documentElement.clientWidth, window.innerWidth || 0),
                    height: Math.max(document.documentElement.clientHeight, window.innerHeight || 0)
                };
            }
        }, {
            key: "resizeChatHeight",
            value: function resizeChatHeight() {
                return this.isMobile() ? this.screenSize().height : Math.min(this._config.chat.height || 500, this.screenSize().height - 30);
            }
        }, {
            key: "createButton",
            value: function createButton() {
                var _this = this;

                var button = document.createElement('button');
                var buttonStyles = this.settings.styles(this.settings).button;
                button.setAttribute('style', buttonStyles);
                this.wrapper.appendChild(button);
                button.setAttribute('id', this.settings.button.elementId); // button.appendChild(document.createTextNode(this.settings.button.label));

                var buttonClicked = function buttonClicked(forceOpen) {
                    if (!_this.isChatCreated()) {
                        _this.createChat(true);

                        setTimeout(function () {
                            _this.sendMessageToIFrame(isChatClosed);
                        }, 1000);
                    }

                    var isChatClosed = _this.chat.style.display === 'none';
                    if (typeof forceOpen === 'boolean' && forceOpen && !isChatClosed) return;

                    if (isChatClosed) {
                        _this.chat.style.display = 'flex';
                        _this.closeButton.style.display = 'flex';
                        _this.button.style.opacity = '0';
                        _this.button.style.transform = "translateY(80%)";
                        setTimeout(function () {
                            _this.chat.style.height = _this.settings.chat.height;
                        });
                    } else {
                        _this.chat.style.height = '0';
                        _this.closeButton.style.display = 'flex';
                        setTimeout(function () {
                            _this.button.style.opacity = '1';
                            _this.button.style.transform = "translateY(0)";
                            _this.chat.style.display = 'none';
                        }, 300);
                    }

                    _this.sendMessageToIFrame(isChatClosed);
                };

                button.onclick = buttonClicked.bind(this);
                this.buttonClick = buttonClicked.bind(this);
                return button;
            }
        }, {
            key: "sendMessageToIFrame",
            value: function sendMessageToIFrame(isChatClosed) {
                this.chat.contentWindow.postMessage("chat open status: ".concat(isChatClosed), '*');
            }
        }, {
            key: "isChatCreated",
            value: function isChatCreated() {
                if (this.wrapper && this.wrapper.children) {
                    var childrenArray = Array.prototype.slice.call(this.wrapper.children);
                    return !!childrenArray.find(function (el) {
                        return el.nodeName === "IFRAME";
                    });
                }

                return false;
            }
        }, {
            key: "isMobile",
            value: function isMobile() {
                return Math.max(document.documentElement.clientWidth, window.innerWidth || 0) < 769;
            }
        }, {
            key: "createChat",
            value: function createChat(addToWrapper) {
                var _this2 = this;

                var iframe = document.createElement('iframe');
                var styles = this.settings.styles(this.settings).chat;
                iframe.setAttribute('style', styles);
                iframe.src = this.settings.chatURL;
                iframe.name = "chat";
                iframe.onload = this.chatLoadingState().bind(this);

                if (this.settings.autoOpen) {
                    setTimeout(function () {
                        return _this2.buttonClick(true);
                    }, this.settings.openDelay * 1000);
                }

                if (addToWrapper) {
                    this.appendChatToWrapper();
                }

                return iframe;
            }
        }, {
            key: "appendChatToWrapper",
            value: function appendChatToWrapper() {
                if (this.settings.position.verticalAlignment === 'top') {
                    this.wrapper.appendChild(this.chat);
                } else {
                    this.wrapper.prepend(this.chat);
                }
            }
        }, {
            key: "start",
            value: function start() {
                var _this3 = this;

                var onDocumentLoad = function onDocumentLoad() {
                    _this3.button = _this3.createButton();
                    _this3.chat = _this3.createChat(); // if (this.isMobile()) {
                    //     this.settings.chat.height = `100%`;
                    // }

                    _this3.wrapper.appendChild(_this3.button);

                    document.body.appendChild(_this3.wrapper);
                };

                window.onload = onDocumentLoad.bind(this);
            }
        }]);

        return LeoChat;
    }();