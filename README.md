## Properties

| Property | Default | Description |
| --- | --- | --- |
| position | `{ horizontalAlignment: 'right', verticalAlignment: 'top' }` | `verticalAlignment['top','bottom']` `horizontalAlignment['left','right']` |
| button | `{ image: 'string', padding: 15', elementId: 'string', width: 300, height: 100,  closeButtonPosition: `right` || `left`}` | `button styles and id tag` |
| chat | `{ width: 300, height: 100 }` | `The chat's window size` |
| autoOpen | `false` | `Should the chat window open automatically` |
| openDelay | `5` | `Number in seconds` |
| chatURL | `string` | `` |
| autoOpen | `boolean` | `should the chat window open automaticaly?` |
| openDelay | `integer` | `delay (in secs) until chat will open (if autoOpen = true)` |
| closeButtonPosition | `string` | options: [`right`, `left`] - set the position of the close button |

## Example
```
<script src="index.js"></script>
<script>
    var chat = new LeoChat({
        position: {
            horizontalAlignment: 'right',
            verticalAlignment: 'top'
        },
        button: {
            padding: 15,
            elementId: 'leo-chat',
            width: 300,
            height: 100,
        },
        chat: {
            width: 700,
            height: 500
        },
        autoOpen: false,
        openDelay: 0,
        chatURL: 'https://webchat-stg.leochats.com/widget/?account=demobot&agent=unicorn'
    });
    chat.start();
</script>
```

## ES6 & Support
```
index.es6.js contains the original code in es6 specs. The transpiled version is in index.js
```